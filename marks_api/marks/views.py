from rest_framework import generics, viewsets
from .models import Mark
from .serializers import MarkSerializer
from rest_framework.response import Response 
from rest_framework.permissions import IsAuthenticatedOrReadOnly


# class AllMarks(generics.ListCreateAPIView):
class AllMarks(viewsets.ModelViewSet):
    queryset = Mark.objects.all()
    serializer_class = MarkSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    